import React from 'react'
import { Button, StyleSheet, Text, View } from 'react-native'

export interface Props {
  name: string
  enthusiasmLevel?: number
}

interface State {
  enthusiasmLevel: number
}

export class Hello extends React.Component<Props, State> {
  constructor(props: Props) {
    super(props);

    if ((props.enthusiasmLevel || 0) <= 0) {
      throw new Error("You could be a little more enthusiastic. :D")
    }

    this.state = {
      enthusiasmLevel: props.enthusiasmLevel || 1
    }
  }

  onIncrement = () => this.setState({ enthusiasmLevel: this.state.enthusiasmLevel + 1 });
  onDecrement = () => {
    let level = this.state.enthusiasmLevel == 0 ? 0 : this.state.enthusiasmLevel - 1;
    this.setState({ enthusiasmLevel: level });
  };

  getExclamationMarks = (numChars: number) => Array(numChars + 1).join("!");

  render() {
    return (
      <View style={styles.root}>
        <Text style={styles.greeting} testID={"helloLabel"}>
          Hello {this.props.name + this.getExclamationMarks(this.state.enthusiasmLevel)}
        </Text>

        <View style={styles.buttons}>
          <View style={styles.button}>
            <Button title="-" onPress={this.onDecrement} testID={"decrementBtn"}/>
        </View>

          <View style={styles.button}>
            <Button title="+" onPress={this.onIncrement} testID={"incrementBtn"}/>
          </View>
        </View>
      </View>
    )
  }
}

// styles
const styles = StyleSheet.create({
  root: {
    alignItems: "center",
    alignSelf: "center"
  },
  buttons: {
    paddingHorizontal: 100,
    flexDirection: "row",
    alignSelf: "center",
    borderWidth: 0
  },
  button: {
    flex: 1,
    paddingHorizontal: 2
  },
  greeting: {
    fontSize: 20,
    color: "#000000",
    fontWeight: "bold"
  }
});
