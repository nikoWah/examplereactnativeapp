module.exports = {
    preset: "react-native",
    moduleFileExtensions: [
        "ts",
        "tsx",
        "js"
    ],
    transform: {
        "^.+\\.(js)$": "<rootDir>/node_modules/babel-jest",
        "\\.(ts|tsx)$": "<rootDir>/node_modules/ts-jest/preprocessor.js"
    },
    testRegex: "/e2e_appium/(.*|\\.(test|spec))\\.(ts|tsx|js)$",
    testPathIgnorePatterns: [
        "\\.snap$",
        "<rootDir>/node_modules/",
        "<rootDir>/lib/",
        "/e2e_appium/config/"
    ],
    cacheDirectory: ".jest/cache"
};