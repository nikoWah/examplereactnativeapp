# Sequelizexpressts

## Setup

1. yarn install
2. node_modules/.bin/sequelize db:create
3. node_modules/.bin/sequelize db:migrate
4. node_modules/.bin/sequelize db:seed:all

#### Postgres
Download and install postgresapp. It automatically creates a user for you that matches your system username.

## Develop

```yarn start``` in the background

## Usage
### Register
```
POST /register
{"email": "hello@me.com", "password": "sdsdfsdfsdfsdf", "confirmPassword": "sdsdfsdfsdfsdf"}
```

### Login
```
POST /login
{"email": "hell2o@me.com", "password": "sdsdfsdfsdfsdf"}
```

### Access Protected Resource
```
GET /some-protected-resource?token=<token-from-login-response>
```
