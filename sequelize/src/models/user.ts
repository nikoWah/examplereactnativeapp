import * as Sequelize from "sequelize";

export interface AddUserAttributes {
  email: string
  password: string
}

export interface UserAttributes {
  id?: number;
  email: string;
  password: string;
  createdAt?: string;
  updatedAt?: string;
}

export type UserInstance = Sequelize.Instance<UserAttributes> & UserAttributes;

export default (sequalize: Sequelize.Sequelize) => {
  const attributes: SequelizeAttributes<UserAttributes> = {
    id: { type: Sequelize.INTEGER, primaryKey: true, autoIncrement: true },
    email: { type: Sequelize.STRING, allowNull: false },
    password: { type: Sequelize.STRING, allowNull: false },
  };
  return sequalize.define<UserInstance, UserAttributes>("User", attributes);
};
