import {getDriver} from './config/testConfig';

let driver;

beforeAll(async () => {
  driver = getDriver();
});

test('Increment/Decrement counter test', async () => {
  let welcomeText = await driver.elementByAccessibilityId("helloLabel");
  let incrementButton = await driver.elementByAccessibilityId("incrementBtn");
  let decrementButton = await driver.elementByAccessibilityId("decrementBtn");

  await expect(welcomeText.text()).resolves.toBe("Hello World!");

  await incrementButton.click();
  await expect(welcomeText.text()).resolves.toBe("Hello World!!");

  await decrementButton.click();
  await expect(welcomeText.text()).resolves.toBe("Hello World!");

  await decrementButton.click();
  await expect(welcomeText.text()).resolves.toBe("Hello World");

  await decrementButton.click();
  await expect(welcomeText.text()).resolves.toBe("Hello World");
});
