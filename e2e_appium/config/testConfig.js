import wd from "wd";

process.env.DEVICE_TYPE = process.env.DEVICE_TYPE || 'iOS';
jasmine.DEFAULT_TIMEOUT_INTERVAL = 60000;

const APPIUM_PORT = 4723;
const APP_NAME = 'ExampleProject';

const APP_PATH = process.env.DEVICE_TYPE === 'iOS' ?
        `ios/build/Build/Products/Debug-iphonesimulator/${APP_NAME}.app` :
        `android/app/build/outputs/apk/app-debug.apk`;

const opts = {
    platformName: process.env.DEVICE_TYPE,
    deviceName: process.env.DEVICE_TYPE === 'iOS' ? 'iPhone 7' : 'Android Emulator',
    app: APP_PATH,
    noReset: true,
    automationName: process.env.DEVICE_TYPE === 'iOS' ? 'XCUITest' : 'UiAutomator2',
};

export function getDriver() {
    const driver = wd.promiseChainRemote('localhost', APPIUM_PORT);
    return driver.init(opts);
}